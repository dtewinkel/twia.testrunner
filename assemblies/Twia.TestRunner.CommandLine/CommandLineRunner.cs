﻿namespace Twia.TestRunner.CommandLine
{
    /// <summary>
    /// Start-up type for the command-line application.
    /// </summary>
    public class CommandLineRunner
    {
        /// <summary>
        /// Run the application.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        /// <returns>The results of the run. 0 for success. Negative value for application error. Positive value for the number of failed tests.</returns>
        public int Run(string[] args)
        {
            return -1;
        }
    }
}