﻿using Twia.TestRunner.CommandLine;

namespace Twia.TestRunner.x86
{
    class Program
    {
        static int Main(string[] args)
        {
            CommandLineRunner runner = new CommandLineRunner();
            return runner.Run(args);
        }
    }
}
