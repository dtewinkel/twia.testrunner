﻿using Twia.TestRunner.CommandLine;

namespace Twia.TestRunner
{
    class Program
    {
        static int Main(string[] args)
        {
            CommandLineRunner runner = new CommandLineRunner();
            return runner.Run(args);
        }
    }
}
